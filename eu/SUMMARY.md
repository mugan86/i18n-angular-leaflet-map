# Table of contents

* [⚙ Instalación / Configuración](README.md)
* [📝 Demos / Ejemplos](demos-ejemplos.md)

## Aspectos Básicos

* [Mapa Básico](aspectos-basicos/mapa-basico.md)
* [Personalizar tamaño](aspectos-basicos/personalizar-tamano.md)
* [Centrar en ubicación](aspectos-basicos/centrar-en-ubicacion.md)

## Zoom

* [Posición / Textos botones](zoom/positions-btn-texts.md)
